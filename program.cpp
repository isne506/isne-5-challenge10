#include <iostream>

using namespace std;

const int row = 6; //Use with the width and length of array
const int col = 7;

void display(int[][7], int); //Function that display the grid of connect4 game
int playerSwitch(int &); // Function that swithc player 1 to player 2 and player 2 to player 1
int winCheck(int[][7], int); // Function that check for winner

void main() {
	int grid[row][col] = {};
	int columnChoose, win = 0, player = 1;
	display(grid, player);

	do {
		cout << "Player " << player << " turn " << "Add column : ";
		cin >> columnChoose; //prompt user to enter the column
		cout << endl;
		for (int i = 5; i >= 0; i--) { //loop for checking a lower column
			if (grid[i][columnChoose - 1] == 0) {
				grid[i][columnChoose - 1] = player;
				break;
			}
		}
		display(grid, player);
		win = winCheck(grid, player);
		player = playerSwitch(player);
	} while (win == 0);

	if (win == 1) {  //if win != 0 will show the winner
		cout << "player 1 win !" << endl;
	}
	if (win == 2) {
		cout << "player 2 win !" << endl;
	}
	system("pause");
}

void display(int grid[][7], int player) {

	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (grid[i][j] == 1)
				cout << "[ " << "X" << " ]";
			else if (grid[i][j] == 2)
				cout << "[ " << "O" << " ]";
			else
				cout << "[ " << " " << " ]";
		}
		cout << endl << endl;
	}
	cout << "  1    2    3    4    5    6    7  " << endl;
}

int playerSwitch(int &player) {
	if (player == 1) {
		return 2;
	}
	else {
		return 1;
	}
}

int winCheck(int grid[][7], int player) {
	for (int i = 5; i > 0; i--) {
		for (int j = 0; j < 6; j++) {
			if (grid[i][j] == player) {
				if (grid[i][j + 1] == player) {
					if (grid[i][j + 2] == player) {
						if (grid[i][j + 3] == player) {
							return player;
						}
					}
				}
			}
		}
	}

	for (int i = 5; i > 0; i--) {
		for (int j = 0; j < 6; j++) {
			if (grid[i][j] == player) {
				if (grid[i - 1][j] == player) {
					if (grid[i - 2][j] == player) {
						if (grid[i - 3][j] == player) {
							return player;
						}
					}
				}
			}
		}
	}
	for (int i = 5; i > 0; i--) {
		for (int j = 0; j < 6; j++) {
			if (grid[i][j] == player) {
				if (grid[i - 1][j + 1] == player) {
					if (grid[i - 2][j + 2] == player) {
						if (grid[i - 3][j + 3] == player) {
							return player;
						}
					}
				}
			}
		}
	}
	for (int i = 5; i > 0; i--) {
		for (int j = 0; j < 6; j++) {
			if (grid[i][j] == player) {
				if (grid[i - 1][j - 1] == player) {
					if (grid[i - 2][j - 2] == player) {
						if (grid[i - 3][j - 3] == player) {
							return player;
						}
					}
				}
			}
		}
	}
	return 0;
}